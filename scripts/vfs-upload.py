#!/usr/bin/env python3

## Copyright (C) 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## NAME
##
##   vfs-upload - upload project file to a VFS server
##
## SYNOPSIS
##
##   vfs-upload VFS-URL [PROJECT-FPATHS ...]
##
## DESCRIPTION
##
##   Upload, or update, project files in a VFS server.  It outputs the
##   project UUID, revision number, and file paths separated by tabs.
##
##   If any file fails to upload will be skipped with a notice in
##   stderr (so that stdout is still safe to use) and cause exit code
##   to be non-zero.
##
##   The file path is the *last* column of the output and it is the
##   last column because it's the one whose value we have no control
##   over and it may contain tabs which would cause issues for any
##   automatic use of the output (file path may also include newlines
##   but if anyone has newlines on their file paths, they're looking
##   for problems, in which case we're happy to oblige).
##
## EXAMPLES
##
##   Upload 3 projects to the VGG public VGFS server and ouput shared
##   project UUID:
##
##       vfs-upload.py \
##           https://meru.robots.ox.ac.uk/store/ \
##           project-1.json \
##           project-2.json \
##           project-3.json \
##
## TODO (maybe):
##
##   We decide whether to create a new project or update an existing
##   one based on whether there's already shared project details on
##   the file.  But it may be interesting to force creation of a new
##   shared project instead of updating.  But that can be done
##   trivially on shell with:
##
##       jq '.project.shared_fid |= "__FILE_ID__"' \
##           | jq '.project.shared_rev |= "__FILE_REV_ID__"' \
##           | jq '.project.shared_rev_timestamp |= "__FILE_REV_TIMESTAMP__"'
##
##   but then again, this script can be done trivially on shell as
##   well and it is written anyway :)

import argparse
import json
import logging
import re
import sys
from typing import Dict, List

import requests

_logger = logging.getLogger()


MAGIC_FID = "__FILE_ID__"
MAGIC_REV = "__FILE_REV_ID__"
MAGIC_TIMESTAMP = "__FILE_REV_TIMESTAMP__"


def vfs_create_project(vfs_url: str, json_project: Dict) -> Dict:
    """Upload to VFS server (creating a new project)"""
    response = requests.post(vfs_url, json=json_project)
    if response.status_code != requests.codes.ok:
        raise Exception(response.reason)
    return response.json()


def vfs_update_project(vfs_url: str, json_project: Dict) -> Dict:
    rev = json_project["project"]["shared_rev"]
    uuid = json_project["project"]["shared_fid"]
    sep = "" if vfs_url.endswith("/") else "/"
    url = f"{vfs_url}{sep}{uuid}?rev={rev}"

    # Reset these (they'll be updated by the VFS server)
    json_project["project"]["shared_rev"] = MAGIC_REV
    json_project["project"]["shared_rev_timestamp"] = MAGIC_TIMESTAMP

    response = requests.post(url, json=json_project)
    if response.status_code != requests.codes.ok:
        raise Exception(response.reason)
    return response.json()


def is_uuid(maybe_uuid: str) -> bool:
    return bool(
        re.fullmatch(
            "[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}",
            maybe_uuid,
        )
    )


def is_str_int(maybe_int: str) -> bool:
    return bool(re.fullmatch("[0-9]+", maybe_int))


def is_vfs_project(json_project: Dict) -> bool:
    for required_key in ["shared_fid", "shared_rev", "shared_rev_timestamp"]:
        if required_key not in json_project["project"]:
            _logger.warn(
                "required key .project.{%s} does not exist", required_key
            )
            return False

    fid = json_project["project"]["shared_fid"]
    rev = json_project["project"]["shared_rev"]
    timestamp = json_project["project"]["shared_rev_timestamp"]

    if not (
        is_uuid(fid) and is_str_int(rev) and is_str_int(timestamp)
    ) and not (
        fid == MAGIC_FID and rev == MAGIC_REV and timestamp == MAGIC_TIMESTAMP
    ):
        _logger.warn("invalid shared fid, rev, and rev_timestamp combination")
        return False

    return True


def is_new_project(json_project: Dict) -> bool:
    fid = json_project["project"]["shared_fid"]
    rev = json_project["project"]["shared_rev"]
    timestamp = json_project["project"]["shared_rev_timestamp"]
    return (
        fid == MAGIC_FID and rev == MAGIC_REV and timestamp == MAGIC_TIMESTAMP
    )


def do_one_project(vfs_url: str, fpath: str) -> str:
    with open(fpath, "r") as fh:
        json_project = json.load(fh)

    if not is_vfs_project(json_project):
        raise Exception(f"File {fpath} is not a valid project file")

    if is_new_project(json_project):
        return vfs_create_project(vfs_url, json_project)
    else:
        return vfs_update_project(vfs_url, json_project)


def main(argv: List[str]) -> int:
    logging.basicConfig()

    argv_parser = argparse.ArgumentParser()
    argv_parser.add_argument(
        "vfs_url",
        help="URL of the VFS server",
    )
    argv_parser.add_argument(
        "project_fpaths",
        nargs="+",
        help="Filepath for the projects to be uploaded",
    )
    args = argv_parser.parse_args(argv[1:])

    if not args.vfs_url.endswith("/"):
        # Not sure if this is a bug in VFS or in the our web server
        # configuration.
        raise Exception(f"VFS URL '{args.vfs_url}' needs to end with '/'")

    all_good = True
    for fpath in args.project_fpaths:
        try:
            response = do_one_project(args.vfs_url, fpath)
        except Exception as ex:
            all_good = False
            print(f"Failed to upload '{fpath}': {ex}", file=sys.stderr)
        else:
            uuid = response["shared_fid"]
            rev = response["shared_rev"]
            print(f"{uuid}\t{rev}\t{fpath}")

    return 0 if all_good else 1


if __name__ == "__main__":
    sys.exit(main(sys.argv))
