# Check that the revision-id in "latest" matches the highest revision in "rev/"
# For example, if "latest/2f/2fb08df3-1513-4940-b152-0b100d0d1bf8" contains "rev":4
# then "rev/2f/2fb08df3-1513-4940-b152-0b100d0d1bf8/rev-4" file must exist.
#
# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# 15 Nov. 2020

import os
import json

LATEST_DIR = "/nvme/via/store/3.x.y/latest"
REV_DIR = "/nvme/via/store/3.x.y/rev"

success_count = 0
file_count = 0
for root, dirs, files in os.walk(LATEST_DIR):
    for file in files:
        project_fn = os.path.join(root, file)
        with open(project_fn, 'r') as f:
            file_count = file_count + 1
            via3 = json.load(f)
            revid = via3['project']['rev']
            latest_rev_fn = os.path.join(REV_DIR, file[0:2], file, 'rev-' + str(revid) + '.json')
            if os.path.isfile(latest_rev_fn):
                success_count = success_count + 1
            else:
                print('FAILED: %s : %s : %s' % (file, latest_rev_fn, revid))

print('Successfully verified %d / %d files' % (success_count, file_count))
