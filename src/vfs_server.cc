// VIA Project Server
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// 19 June 2019
//
// This code builds on the following example included in the boost library:
// https://www.boost.org/doc/libs/1_70_0/libs/beast/example/advanced/server/advanced_server.cpp

#include "config.h"
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/strand.hpp>
#include <boost/make_unique.hpp>
#include <boost/optional.hpp>

#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include <boost/filesystem.hpp>

#include <cassert>
#include "leveldb/db.h"

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <sstream>
#include <exception>
#include <chrono>
#include <ctime>

namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                    // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>

boost::uuids::random_generator uuid_generator;
leveldb::DB* revdb;

const std::string VFS_SERVER_NAME = VFS_PROJECT_SERVER_NAME "-" VFS_PROJECT_SERVER_VERSION_MAJOR "." VFS_PROJECT_SERVER_VERSION_MINOR "." VFS_PROJECT_SERVER_VERSION_PATCH;
const std::string VFS_SERVER_AUTHOR = VFS_PROJECT_SERVER_AUTHOR_NAME " (" VFS_PROJECT_SERVER_AUTHOR_EMAIL "), " VFS_PROJECT_SERVER_CURRENT_RELEASE_DATE;

const std::size_t PID_LENGTH = 36;
const std::string URL_REV_PARAM_MARKER = "?rev=";
const std::string PID_MARKER = "__FILE_ID__";
const std::string REV_MARKER = "__FILE_REV_ID__";
const std::string REV_TIMESTAMP_MARKER = "__FILE_REV_TIMESTAMP__";

std::ofstream perf_log_f;
std::ofstream dbg_log_f;

// This function produces an HTTP response for the given
// request. The type of the response object depends on the
// contents of the request, so the interface requires the
// caller to pass a generic lambda for receiving the response.
template<
  class Body, class Allocator,
  class Send>
void
handle_request(
               beast::string_view project_dir,
               beast::string_view project_rev_dir,
               http::request<Body, http::basic_fields<Allocator>>&& req,
               Send&& send)
{
  auto request_timestamp = std::chrono::high_resolution_clock::now();

  // Returns a bad request response
  auto const bad_request =
    [&req](beast::string_view why)
    {
      http::response<http::string_body> res{http::status::bad_request, req.version()};
      res.set(http::field::content_type, "text/html");
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.body() = std::string(why);
      res.prepare_payload();
      return res;
    };

  // Returns a not found response
  auto const not_found =
    [&req](beast::string_view target)
    {
      http::response<http::string_body> res{http::status::not_found, req.version()};
      res.set(http::field::content_type, "text/html");
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.body() = "unknown project: '" + std::string(target);
      res.prepare_payload();
      return res;
    };

  // Returns a server error response
  auto const server_error =
    [&req](beast::string_view what)
    {
      http::response<http::string_body> res{http::status::internal_server_error, req.version()};
      res.set(http::field::content_type, "text/html");
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.body() = "An error occurred: '" + std::string(what) + "'";
      res.prepare_payload();
      return res;
    };

  // Returns the server info
  auto const server_info =
    [&req]()
    {
      http::response<http::string_body> res{http::status::ok, req.version()};
      res.set(http::field::content_type, "text/plain;charset=UTF-8");
      res.set(http::field::server, VFS_SERVER_NAME);
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.body() = VFS_SERVER_NAME;
      res.prepare_payload();
      return res;
    };

  // Returns a project info
  auto const project_info =
    [&req](beast::string_view pid, beast::string_view rev, beast::string_view rev_timestamp )
    {
      http::response<http::string_body> res{http::status::ok, req.version()};
      res.set(http::field::content_type, "application/json");
      res.set(http::field::server, VFS_SERVER_NAME);
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.body() = "{\"shared_fid\":\"" + std::string(pid) + "\",\"shared_rev\":\"" + std::string(rev) + "\",\"shared_rev_timestamp\":\"" + std::string(rev_timestamp) + "\"}";
      res.prepare_payload();
      return res;
    };
  // debug log
  //std::cout << req.method_string() << " " << req.target() << ": " << req.body().size() << "bytes\n" << std::flush;

  // Make sure we can handle the method
  if( req.method() != http::verb::get &&
      req.method() != http::verb::post &&
      req.method() != http::verb::head)
    return send(bad_request("Unknown HTTP-method"));

  // Request path must be absolute and not contain "..".
  if( req.target().empty() ||
      req.target()[0] != '/' ||
      req.target().find("..") != beast::string_view::npos)
    return send(bad_request("Illegal request-target"));

  try {
    leveldb::Status leveldb_status;
    // Build the path to the requested file
    if( req.method() == http::verb::post ) {
      std::size_t param_rev_index = req.target().find(URL_REV_PARAM_MARKER);
      if ( param_rev_index == beast::string_view::npos ) {
        // initialize a new VIA project
        std::string new_rev_str = "1";
        std::string pid = boost::uuids::to_string( uuid_generator() );

        std::ostringstream ss;
        ss << req.body();
        std::string payload = ss.str();

        // update project id
        std::size_t pidmarker_index = payload.find(PID_MARKER);
        if ( pidmarker_index == std::string::npos ) {
          return send(bad_request("missing " + PID_MARKER));
        }
        payload.replace(pidmarker_index, PID_MARKER.length(), pid);

        // update revision id
        std::size_t revmarker_index = payload.find(REV_MARKER);
        if ( revmarker_index == std::string::npos ) {
          return send(bad_request("missing " + REV_MARKER));
        }
        payload.replace(revmarker_index, REV_MARKER.length(), new_rev_str);

        // update revision timestamp
        std::size_t revtsmarker_index = payload.find(REV_TIMESTAMP_MARKER);
        if ( revtsmarker_index == std::string::npos ) {
          return send(bad_request("missing " + REV_TIMESTAMP_MARKER));
        }
        std::ostringstream time_since_epoch_ss;
        auto request_timestamp_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(request_timestamp);
        auto rev_timestamp_value = request_timestamp_ms.time_since_epoch();
        time_since_epoch_ss << rev_timestamp_value.count();
        std::string rev_timestamp_str = time_since_epoch_ss.str();
        payload.replace(revtsmarker_index, REV_TIMESTAMP_MARKER.length(), rev_timestamp_str);

        // write to file
        std::string file_path = std::string(project_dir) + "/" + pid.substr(0,2) + "/" + pid;
        std::ofstream latest( file_path );
        if ( ! latest.is_open() ) {
          return send(bad_request("failed to save file"));
        }
        latest << payload;
        latest.close();
        leveldb_status = revdb->Put(leveldb::WriteOptions(), pid, new_rev_str);
        if ( ! leveldb_status.ok() ) {
          return send(bad_request("failed to save revision"));
        }

        // save a copy to revision folder
        std::string rev_path_str = std::string(project_rev_dir) + "/" + pid.substr(0,2) + "/" + pid + "/";
        std::string rev_file_str = std::string(project_rev_dir) + "/" + pid.substr(0,2) + "/" + pid + "/rev-" + new_rev_str + ".json";
        boost::filesystem::path rev_path(rev_path_str);
        boost::filesystem::create_directories( boost::filesystem::path(rev_path_str) );
        boost::filesystem::copy_file( boost::filesystem::path(file_path), boost::filesystem::path(rev_file_str) );

        // send response
        return send(project_info(pid, new_rev_str, rev_timestamp_str));
      } else {
        // update existing project
        std::string request_rev_str(req.target().substr(param_rev_index + URL_REV_PARAM_MARKER.length()));
        std::size_t last_slash = req.target().rfind("/");
        if ( last_slash == std::string::npos ) {
          return send(bad_request("bad target"));
        }
        std::string pid( req.target().substr(last_slash + 1, PID_LENGTH) );
        std::string current_rev_str;
        leveldb_status = revdb->Get(leveldb::ReadOptions(), pid, &current_rev_str);
        if ( ! leveldb_status.ok() ) {
          return send(bad_request("unknown project"));
        }
        if ( current_rev_str != request_rev_str ) {
          return send(bad_request("bad revision (latest=" + current_rev_str + ")"));
        }

        // convert revision-id string to integer
        std::istringstream revss1(current_rev_str);
        unsigned int current_rev;
        revss1 >> current_rev;
        unsigned int new_rev = current_rev + 1;
        std::ostringstream revss2;
        revss2 << new_rev;
        std::string new_rev_str = revss2.str();

	// @todo: ensure that pid in json matches the pid in http request

        // update revision id in payload
        std::ostringstream ss;
        ss << req.body();
        std::string payload = ss.str();
        std::size_t revmarker_index = payload.find(REV_MARKER);
        if ( revmarker_index == std::string::npos ) {
          return send(bad_request("missing " + REV_MARKER));
        }
        payload.replace(revmarker_index, REV_MARKER.length(), new_rev_str);

        // update revision timestamp
        std::size_t revtsmarker_index = payload.find(REV_TIMESTAMP_MARKER);
        if ( revtsmarker_index == std::string::npos ) {
          return send(bad_request("missing " + REV_TIMESTAMP_MARKER));
        }
        std::ostringstream time_since_epoch_ss;
        auto request_timestamp_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(request_timestamp);
        auto rev_timestamp_value = request_timestamp_ms.time_since_epoch();
        time_since_epoch_ss << rev_timestamp_value.count();
        std::string rev_timestamp_str = time_since_epoch_ss.str();
        payload.replace(revtsmarker_index, REV_TIMESTAMP_MARKER.length(), rev_timestamp_str);

        // write to file
        std::string file_path = std::string(project_dir) + "/" + pid.substr(0,2) + "/" + pid;
        std::ofstream latest( file_path );
        if ( ! latest.is_open() ) {
          return send(bad_request("failed to save file"));
        }
        latest << payload;
        latest.close();
        leveldb_status = revdb->Put(leveldb::WriteOptions(), pid, new_rev_str);
        if ( ! leveldb_status.ok() ) {
          return send(bad_request("failed to save revision"));
        }

        // save a copy to revision folder
        std::string rev_file_str = std::string(project_rev_dir) + "/" + pid.substr(0,2) + "/" + pid + "/rev-" + new_rev_str + ".json";
        boost::filesystem::copy_file( boost::filesystem::path(file_path), boost::filesystem::path(rev_file_str) );
	
        // send response
        return send(project_info(pid, new_rev_str, rev_timestamp_str));
      }
    } else {
      // Attempt to open the file
      std::size_t last_slash = req.target().rfind("/");
      if ( last_slash == std::string::npos ) {
        return send(bad_request("bad target"));
      }
      if ( (last_slash + 1) == req.target().length() ) {
        return send(server_info());
      }

      std::size_t param_marker = req.target().rfind("?rev=");
      bool get_old_rev = false;
      std::string old_rev_id_str = "";
      if ( param_marker != std::string::npos ) {
        get_old_rev = true;
        old_rev_id_str = std::string(req.target().substr(param_marker+5)); // "?rev=2
        if(old_rev_id_str.length() > 4) {
          return send(bad_request("invalid rev id"));
        }
        std::istringstream ss(old_rev_id_str);
        int old_rev_id;
        ss >> old_rev_id;
        if(old_rev_id < 1 || old_rev_id > 9999) {
          return send(bad_request("invalid rev id"));
        }
      }

      if ( !get_old_rev &&
           (last_slash + PID_LENGTH + 1) != req.target().length() ) {
        return send(not_found( req.target().substr(last_slash+1) ));
      }

      std::string pid( req.target().substr(last_slash + 1, PID_LENGTH) );
      std::string current_rev_str;
      leveldb_status = revdb->Get(leveldb::ReadOptions(), pid, &current_rev_str);
      if ( ! leveldb_status.ok() ) {
        return send(not_found( req.target().substr(last_slash+1) ));
      }

      std::string file_path = std::string(project_dir) + "/" + pid.substr(0,2) + "/" + pid;
      if(get_old_rev) {
        file_path = std::string(project_rev_dir) + "/" + pid.substr(0,2) + "/" + pid + "/rev-" + old_rev_id_str + ".json";
      }
      beast::error_code ec;
      http::file_body::value_type body;
      body.open(file_path.c_str(), beast::file_mode::scan, ec);

      // Handle the case where the file doesn't exist
      if(ec == beast::errc::no_such_file_or_directory)
        return send(not_found(req.target()));

      // Handle an unknown error
      if(ec)
        return send(server_error(ec.message()));

      // Cache the size since we need it after the move
      auto const size = body.size();

      // Respond to HEAD request
      if(req.method() == http::verb::head)
        {
          http::response<http::empty_body> res{http::status::ok, req.version()};
          res.set(http::field::content_type, "application/json");
          res.set(http::field::access_control_allow_origin, "*");
          res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
          res.content_length(size);
          return send(std::move(res));
        }

      // Respond to GET request
      http::response<http::file_body> res{ std::piecewise_construct,
          std::make_tuple(std::move(body)),
          std::make_tuple(http::status::ok, req.version())
          };
      res.set(http::field::content_type, "application/json");
      res.set(http::field::access_control_allow_origin, "*");
      res.set(http::field::access_control_allow_methods, "GET, POST, HEAD");
      res.content_length(size);
      return send(std::move(res));
    }
  } catch(std::exception &ex) {
    auto dbg_timestamp_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(request_timestamp);
    auto dbg_timestamp_value = dbg_timestamp_ms.time_since_epoch();
    dbg_log_f << "\n====================[ Exception Start: " << dbg_timestamp_value.count() << " ]====================\n";
    dbg_log_f << "target=" << req.method_string() << " " << req.target() << "\n";
    dbg_log_f << "exception=" << ex.what() << "\n";
    dbg_log_f << "body=" << req.body() << "\n";
    dbg_log_f << "====================[ Exception End  : " << dbg_timestamp_value.count() << " ]====================\n";
    return send(bad_request("failed to process request"));
  }
}

//------------------------------------------------------------------------------

// Report a failure
void
fail(beast::error_code ec, char const* what)
{
  //std::cerr << what << ": " << ec.message() << "\n";
  dbg_log_f << what << ": " << ec.message() << "\n";
}

//------------------------------------------------------------------------------

// Handles an HTTP server connection
class http_session : public std::enable_shared_from_this<http_session>
{
  // This queue is used for HTTP pipelining.
  class queue
  {
    enum
      {
	      // Maximum number of responses we will queue
	      limit = 256
      };

    // The type-erased, saved work item
    struct work
    {
      virtual ~work() = default;
      virtual void operator()() = 0;
    };

    http_session& self_;
    std::vector<std::unique_ptr<work>> items_;

  public:
    explicit
    queue(http_session& self)
      : self_(self)
    {
      static_assert(limit > 0, "queue limit must be positive");
      items_.reserve(limit);
    }

    // Returns `true` if we have reached the queue limit
    bool
    is_full() const
    {
      return items_.size() >= limit;
    }

    // Called when a message finishes sending
    // Returns `true` if the caller should initiate a read
    bool
    on_write()
    {
      BOOST_ASSERT(! items_.empty());
      auto const was_full = is_full();
      items_.erase(items_.begin());
      if(! items_.empty())
        (*items_.front())();
      return was_full;
    }

    // Called by the HTTP handler to send a response.
    template<bool isRequest, class Body, class Fields>
    void
    operator()(http::message<isRequest, Body, Fields>&& msg)
    {
      // This holds a work item
      struct work_impl : work
      {
        http_session& self_;
        http::message<isRequest, Body, Fields> msg_;

        work_impl(
                  http_session& self,
                  http::message<isRequest, Body, Fields>&& msg)
          : self_(self)
          , msg_(std::move(msg))
        {
        }

        void
        operator()()
        {
          http::async_write(
                            self_.stream_,
                            msg_,
                            beast::bind_front_handler(
                                                      &http_session::on_write,
                                                      self_.shared_from_this(),
                                                      msg_.need_eof()));
        }
      };

      // Allocate and store the work
      items_.push_back(
                       boost::make_unique<work_impl>(self_, std::move(msg)));

      // If there was no previous work, start this one
      if(items_.size() == 1) {
	      perf_log_f << std::flush;
	      dbg_log_f << std::flush;
        (*items_.front())();
      }
    }
  };

  beast::tcp_stream stream_;
  beast::flat_buffer buffer_;
  std::shared_ptr<std::string const> project_dir_;
  std::shared_ptr<std::string const> project_rev_dir_;
  queue queue_;

  // The parser is stored in an optional container so we can
  // construct it from scratch it at the beginning of each new message.
  boost::optional<http::request_parser<http::string_body>> parser_;

public:
  // Take ownership of the socket
  http_session(
               tcp::socket&& socket,
               std::shared_ptr<std::string const> const& project_dir,
               std::shared_ptr<std::string const> const& project_rev_dir)
    : stream_(std::move(socket))
    , project_dir_(project_dir)
    , project_rev_dir_(project_rev_dir)
    , queue_(*this)
  {
  }

  // Start the session
  void
  run()
  {
    do_read();
  }

private:
  void
  do_read()
  {
    // Construct a new parser for each message
    parser_.emplace();

    // precaution to prevent abuse
    parser_->body_limit(1048576); // 1MB
    //parser_->body_limit(10485760); // 10MB
    parser_->header_limit(8192);  // 8KB

    // Set the timeout.
    stream_.expires_after(std::chrono::seconds(5));

    // Read a request using the parser-oriented interface
    http::async_read(
                     stream_,
                     buffer_,
                     *parser_,
                     beast::bind_front_handler(
                                               &http_session::on_read,
                                               shared_from_this()));
  }

  void
  on_read(beast::error_code ec, std::size_t bytes_transferred)
  {
    boost::ignore_unused(bytes_transferred);

    // This means they closed the connection
    if(ec == http::error::end_of_stream)
      return do_close();

    if(ec)
      return fail(ec, "read");

    // Send the response
    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    handle_request(*project_dir_, *project_rev_dir_, std::move(parser_->release()), queue_);
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    // collect and log server performance data
    auto request_timestamp_ms = std::chrono::time_point_cast<std::chrono::microseconds>(t0);
    auto timestamp_value = request_timestamp_ms.time_since_epoch();
    auto duration_value = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0);
    perf_log_f << timestamp_value.count() << ","
               << duration_value.count() << "\n";

    // If we aren't at the queue limit, try to pipeline another request
    if(! queue_.is_full() && ! parser_->is_done() )
      do_read();
  }

  void
  on_write(bool close, beast::error_code ec, std::size_t bytes_transferred)
  {
    boost::ignore_unused(bytes_transferred);

    if(ec)
      return fail(ec, "write");

    if(close)
      {
        // This means we should close the connection, usually because
        // the response indicated the "Connection: close" semantic.
        return do_close();
      }

    // Inform the queue that a write completed
    if(queue_.on_write())
      {
        // Read another request
        do_read();
      }
  }

  void
  do_close()
  {
    // Send a TCP shutdown
    beast::error_code ec;
    stream_.socket().shutdown(tcp::socket::shutdown_send, ec);

    // At this point the connection is closed gracefully
  }
};

//------------------------------------------------------------------------------

// Accepts incoming connections and launches the sessions
class listener : public std::enable_shared_from_this<listener>
{
  net::io_context& ioc_;
  tcp::acceptor acceptor_;
  std::shared_ptr<std::string const> project_dir_;
  std::shared_ptr<std::string const> project_rev_dir_;

public:
  listener(
           net::io_context& ioc,
           tcp::endpoint endpoint,
           std::shared_ptr<std::string const> const& project_dir,
           std::shared_ptr<std::string const> const& project_rev_dir)
    : ioc_(ioc)
    , acceptor_(net::make_strand(ioc))
    , project_dir_(project_dir)
    , project_rev_dir_(project_rev_dir)
  {
    beast::error_code ec;

    // Open the acceptor
    acceptor_.open(endpoint.protocol(), ec);
    if(ec)
      {
        fail(ec, "open");
        return;
      }

    // Allow address reuse
    acceptor_.set_option(net::socket_base::reuse_address(true), ec);
    if(ec)
      {
        fail(ec, "set_option");
        return;
      }

    // Bind to the server address
    acceptor_.bind(endpoint, ec);
    if(ec)
      {
        fail(ec, "bind");
        return;
      }

    // Start listening for connections
    acceptor_.listen(
                     net::socket_base::max_listen_connections, ec);
    if(ec)
      {
        fail(ec, "listen");
        return;
      }
  }

  // Start accepting incoming connections
  void
  run()
  {
    do_accept();
  }

private:
  void
  do_accept()
  {
    // The new connection gets its own strand
    acceptor_.async_accept(
                           net::make_strand(ioc_),
                           beast::bind_front_handler(
                                                     &listener::on_accept,
                                                     shared_from_this()));
  }

  void
  on_accept(beast::error_code ec, tcp::socket socket)
  {
    if(ec)
      {
        fail(ec, "accept");
      }
    else
      {
        // Create the http session and run it
        std::make_shared<http_session>(
                                       std::move(socket),
                                       project_dir_, project_rev_dir_)->run();
      }

    // Accept another connection
    do_accept();
  }
};

//------------------------------------------------------------------------------

void init_leveldb(const std::string project_rev_dir) {
  std::cout << "Initializing leveldb from : " << project_rev_dir << std::endl;
  std::string char_list = "0123456789abcdef";
  leveldb::Status leveldb_status;
  unsigned project_count = 0;
  for ( std::size_t i = 0; i < char_list.size(); ++i ) {
    for ( std::size_t j = 0; j < char_list.size(); ++j ) {
      std::stringstream suffix;
      suffix << '/' << char_list[i] << char_list[j];
      boost::filesystem::path shard_dir(project_rev_dir + suffix.str());

      boost::filesystem::directory_iterator end;

      for ( boost::filesystem::directory_iterator it(shard_dir); it != end; ++it ) {
	const boost::filesystem::path shard_project_path = (*it);
	const boost::filesystem::path via_pid_path = shard_project_path.filename();
	const std::string via_pid = via_pid_path.string();
	unsigned int max_rev_id = 0;
	for ( boost::filesystem::directory_iterator revit(shard_project_path); revit != end; ++revit ) {
	  const boost::filesystem::path rev_path = (*revit);
	  const boost::filesystem::path revfn = rev_path.stem();
	  std::string revfn_str = revfn.string();
	  std::string revid_str = revfn_str.substr(4);
	  std::stringstream ss(revid_str);
	  unsigned int rev_id;
	  ss >> rev_id;
	  if ( rev_id > max_rev_id ) {
	    max_rev_id = rev_id;
	  }
	}

	if ( max_rev_id != 0 ) {
	  std::stringstream ss2;
	  ss2 << max_rev_id;
	  std::string max_rev_id_str = ss2.str();
	  leveldb_status = revdb->Put(leveldb::WriteOptions(), via_pid, max_rev_id_str);
	  if ( leveldb_status.ok() ) {
	    //std::cout << via_pid << " : " << max_rev_id << std::endl;
	    project_count = project_count + 1;
	  } else {
	    std::cout << via_pid << " : " << max_rev_id << ": ERROR adding to leveldb" << std::endl;
	  }
	}
      }
    }
  }
  std::cout << "Initialized leveldb with " << project_count << " VIA projects" << std::endl;
}

int main(int argc, char* argv[])
{
  std::string HELP_TEXT = R"TEXT(
Usage: via_project_server <address> <port> <threads> <project_dir> <project_rev_dir> <project_rev_db> <log_dir>

address : server runs on this IP address
port    : server listens to this port
threads : number of concurrent threads used by HTTP server
project_dir     : latest revision of VIA project is stored here
project_rev_dir : old revisions of VIA project is stored here
project_rev_db  : leveldb [key,value] database is stored here
log_dir         : location of output log files)TEXT";

  // Check command line arguments.
  if (argc != 8)
    {
      std::cerr << HELP_TEXT << std::endl;
      return EXIT_FAILURE;
    }
  auto const address = net::ip::make_address(argv[1]);
  auto const port = static_cast<unsigned short>(std::atoi(argv[2]));
  auto const threads = std::max<int>(1, std::atoi(argv[3]));
  auto const project_dir = std::make_shared<std::string>(argv[4]);
  auto const project_rev_dir = std::make_shared<std::string>(argv[5]);
  auto const project_rev_db = std::string(argv[6]);
  auto const log_dir = std::string(argv[7]);

  // init leveldb (key,value) store
  std::cout << "Versioned File Server (" << VFS_SERVER_NAME << ")" << std::endl;
  std::cout << "Author: " << VFS_SERVER_AUTHOR << std::endl;
  std::cout << "\nConfiguration:" << std::endl;
  std::cout << "  threads         = " << threads << std::endl;
  std::cout << "  project_dir     = " << argv[4] << std::endl;
  std::cout << "  project_rev_dir = " << argv[5] << std::endl;
  std::cout << "  project_rev_db  = " << project_rev_db << std::endl;

  // leveldb
  leveldb::Options options;
  options.create_if_missing = true;
  leveldb::Status status = leveldb::DB::Open(options, project_rev_db, &revdb);
  assert(status.ok());
  init_leveldb(*project_rev_dir);

  // performance logs
  time_t rawtime;
  struct tm *timeinfo;
  char tsbuf[80];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(tsbuf, sizeof(tsbuf), "%d%m%Y_%H%M%S", timeinfo);
  boost::filesystem::path logfn(log_dir);
  boost::filesystem::path dbgfn(log_dir);
  logfn = logfn / (std::string(tsbuf) + std::string("_perf.csv"));
  dbgfn = dbgfn / (std::string(tsbuf) + std::string("_debug.csv"));

  perf_log_f.open( logfn.string() );
  dbg_log_f.open( dbgfn.string() );
  if ( ! perf_log_f.is_open() || ! dbg_log_f.is_open() ) {
    std::cerr << "Failed to open performance/debug log files " << std::endl;
    std::cerr << "  - " << logfn << std::endl;
    std::cerr << "  - " << dbgfn << std::endl;
    return EXIT_SUCCESS;
  } else {
    std::cout << "  performance log = " << logfn.string() << std::endl;
    std::cout << "  debug log       = " << dbgfn.string() << std::endl;
  }
  perf_log_f << "microseconds_since_epoch,response_time_in_microseconds" << std::endl;

  // The io_context is required for all I/O
  net::io_context ioc{threads};

  // Create and launch a listening port
  std::make_shared<listener>(
                             ioc,
                             tcp::endpoint{address, port},
                             project_dir, project_rev_dir)->run();

  // Capture SIGINT and SIGTERM to perform a clean shutdown
  net::signal_set signals(ioc, SIGINT, SIGTERM);
  signals.async_wait(
                     [&](beast::error_code const&, int)
                     {
                       // Stop the `io_context`. This will cause `run()`
                       // to return immediately, eventually destroying the
                       // `io_context` and all of the sockets in it.
                       ioc.stop();
                     });

  // Run the I/O service on the requested number of threads
  std::vector<std::thread> v;
  v.reserve(threads - 1);
  for(auto i = threads - 1; i > 0; --i)
    v.emplace_back(
                   [&ioc]
                   {
                     ioc.run();
                   });
  std::cout << "Ready and listening for http requests at " << address << ":" << port << " ..." << std::endl;
  ioc.run();

  // (If we get here, it means we got a SIGINT or SIGTERM)

  delete revdb;
  perf_log_f.close();
  dbg_log_f.close();

  // Block until all the threads exit
  for(auto& t : v)
    t.join();

  return EXIT_SUCCESS;
}
