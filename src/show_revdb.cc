
// VIA Project Server
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// 19 June 2019
//
// This code builds on the following example included in the boost library:
// https://www.boost.org/doc/libs/1_70_0/libs/beast/example/advanced/server/advanced_server.cpp

#include <cassert>
#include "leveldb/db.h"

#include <iostream>

int main(int argc, char* argv[])
{
  // Check command line arguments.
  if (argc != 2)
    {
      std::cerr <<
        "Usage: show_revdb <project_rev_db>\n";
      return EXIT_FAILURE;
    }
  auto const project_rev_db = std::string(argv[1]);

  // leveldb
  leveldb::DB* revdb;
  leveldb::Options options;
  options.create_if_missing = true;
  leveldb::Status status = leveldb::DB::Open(options, project_rev_db, &revdb);

  if ( !status.ok() ) {
    std::cerr << status.ToString() << std::endl;
  }
  assert(status.ok());

  leveldb::Iterator* it = revdb->NewIterator(leveldb::ReadOptions());
  unsigned long pid_count = 0;
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    std::cout << it->key().ToString() << ","  << it->value().ToString() << std::endl;
    pid_count = pid_count + 1;
  }
  if ( !it->status().ok() ) {
    std::cerr << it->status().ToString() << std::endl;
  }
  assert(it->status().ok());  // Check for any errors found during the scan
  std::cout << "project-count," << pid_count << std::endl;
  delete it;
  delete revdb;
  return EXIT_SUCCESS;
}
