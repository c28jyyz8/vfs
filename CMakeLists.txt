##
## Versioned File Server (VFS)
##
## Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
## 2 Oct. 2020
##

cmake_minimum_required( VERSION 3.5 )
project( versioned_file_server )

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

## export version number in configure header file
set (VFS_PROJECT_SERVER_NAME "versioned_file_server")
set (VFS_PROJECT_SERVER_VERSION_MAJOR "0")
set (VFS_PROJECT_SERVER_VERSION_MINOR "0")
set (VFS_PROJECT_SERVER_VERSION_PATCH "1")
set (VFS_PROJECT_SERVER_URL "https://gitlab.com/vgg/vfs")
set (VFS_PROJECT_SERVER_AUTHOR_NAME "Abhishek Dutta")
set (VFS_PROJECT_SERVER_AUTHOR_EMAIL "adutta@robots.ox.ac.uk")
set (VFS_PROJECT_SERVER_FIRST_RELEASE_DATE "02 Oct 2020")
set (VFS_PROJECT_SERVER_CURRENT_RELEASE_DATE "02 Oct 2020")
set( BUILD_SHARED_LIBS "OFF" )

## Dependencies
find_package(Boost 1.70.0 COMPONENTS filesystem REQUIRED)
find_package(leveldb)
find_package(Threads REQUIRED)

add_subdirectory(src)
