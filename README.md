# Versioned File Store

A generic file store for a large number of diverse services at VGG.

## Running with Docker

A `Dockerfile` is provided to build VFS and run it into its own
container.

First build the docker container:

    git clone git@gitlab.com:vgg/vfs.git
    cd vfs
    docker build -t vfs .

Create the directory structure (in the host) where VFS will store its
data:

    scripts/create_bucket_folders.sh /srv/vfs-data

and finally, start the docker container:

    docker run vfs \
        --volume /srv/vfs-data:/srv/vfs-data \
        --publish 127.0.0.1:8000:8000 \
        vfs \
        0.0.0.0 \
        8000 \
        2 \
        /srv/vfs-data/latest \
        /srv/vfs-data/rev \
        /srv/vfs-data/revdb \
        /srv/vfs-data/log

You should now be able to connect to VFS:

    $ wget --quiet --output-document - http://127.0.0.1:8000
    versioned_file_server-0.0.1

The above exposes VFS on the local network only.  You probably should
then setup a web server, either on the host or another container, in a
reverse proxy configuration, and control access to VFS from it and
impose limits such as maximum file size.


## Building from source

VFS has the following dependencies:

- C++ compiler
- CMake build system
- Boost Asio, Beast, Filesystem, Optional, and UUID libraries
- LevelDB

which should be available for installation in all Linux distribution
package managers.  To build with CMake:

    git clone git@gitlab.com:vgg/vfs.git
    cd vfs
    mkdir build \
    cd build \
    cmake ..
    make

## Running VFS

Currently, there is no `install` target and `vfs_server` needs to run
from the build directory, like so:

    ./src/vfs_server <address> <port> <threads> <project_dir> <project_rev_dir> <project_rev_db> <log_dir>

    address         : server runs on this IP address
    port            : server listens to this port
    threads         : number of concurrent threads used by HTTP server
    project_dir     : latest revision of VIA project is stored here
    project_rev_dir : old revisions of VIA project is stored here
    project_rev_db  : leveldb [key,value] database is stored here
    log_dir         : location of output log files

Before running VFS for the first time, the file structure in the
project data directory needs to be setup.  This can be done with
`scripts/create_bucket_folders.sh`, like so:

    scripts/create_bucket_folders.sh /srv/vfs-data/  # only needs to be ran the first time

Finally, `vfs_server` can start (run this from the build directory):

    ./src/vfs_server \
        0.0.0.0 \
        9669 \
        8 \
        /srv/vfs-data/latest \
        /srv/vfs-data/rev \
        /srv/vfs-data/revdb \
        /srv/vfs-data/log


## Protocol

POST requests can be made to create a new project or update an
existing project in the VFS server.  GET requests can be made to
retrieve any project revision number.

A successful query will return a JSON object with the following keys:

- `.shared_fid`
- `.shared_rev`
- `.shared_rev_timestamp`

When making POST requests, the body of the request should be the JSON
object to store.  The three keys above must exist under the `.project`
key, i.e., these must exist:

- `.project.shared_fid`
- `.project.shared_rev`
- `.project.shared_rev_timestamp`


### Create New Project

```
==================================[ Request ]===================================
POST * HTTP/1.1
Accept: application/json
Content-Length: 2278
Content-Type: application/json
Host: localhost:9669

{
  "project": {
    "shared_fid": "__FILE_ID__",                      ## this marker must be present
    "shared_rev": "__FILE_REV_ID__",                  ## this marker must be present
    "shared_rev_timestamp": "__FILE_REV_TIMESTAMP__", ## this marker must be present
    ...
  },
  ...
}

=================================[ Response ]===================================
HTTP/1.1 200 OK
Content-Type: application/json
Server: via_project_server-0.0.1
Content-Length: 56

{"shared_fid":"9872e297-5740-4326-ad81-5105fa68068d","shared_rev":"1","shared_rev_timestamp":"1682506281377"}
```

### Update existing project

```
==================================[ Request ]===================================
POST /f9bae8dc-c8df-4cc6-8d23-a730b400d8a3?rev=1 HTTP/1.1
Host: localhost:9669
Content-Length: 1400
Content-Type: application/json

{
  "project": {
    "shared_fid": "f9bae8dc-c8df-4cc6-8d23-a730b400d8a3",## must contain the project-id
    "shared_rev": "__FILE_REV_ID__",                     ## this marker must be present
    ...
  },
  ...
}

=================================[ Response ]===================================
HTTP/1.1 200 OK
Content-Type: application/json
Server: via_project_server-0.0.1
Content-Length: 56

{"shared_fid":"f9bae8dc-c8df-4cc6-8d23-a730b400d8a3","shared_rev":"2"}
```

### Fetch latest revision of a project

```
==================================[ Request ]===================================
GET /f9bae8dc-c8df-4cc6-8d23-a730b400d8a3 HTTP/1.1
Host: localhost:9669

=================================[ Response ]===================================
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 1379

{
  "project": {
    "shared_fid": "f9bae8dc-c8df-4cc6-8d23-a730b400d8a3",
    "shared_rev": "2",                                    ## (default) latest revision
    ...
  },
  ...
}
```

### Fetch revision of a project

```
==================================[ Request ]===================================
GET /f9bae8dc-c8df-4cc6-8d23-a730b400d8a3?rev=1 HTTP/1.1
Host: localhost:9669

=================================[ Response ]===================================
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 1379

{
  "project": {
    "shared_fid": "f9bae8dc-c8df-4cc6-8d23-a730b400d8a3",
    "shared_rev": "1",                                    ## old revision (i.e. rev=1)
    ...
  },
  ...
}
```

## Authors

- Abhishek Dutta <adutta@robots.ox.ac.uk>
- David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
